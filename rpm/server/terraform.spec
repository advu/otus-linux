Summary:	Terraform
Name:		terraform
Version:	0.11.7
Release:	1
License:	Mozilla Public License, version 2.0
URL:		https://github.com/hashicorp/terraform
Group:		Sistem/Server
BuildArch:	x86_64

%description
Terraform

%prep
curl -o %{name}-%{version}.zip https://releases.hashicorp.com/terraform/%{version}/terraform_%{version}_linux_amd64.zip

%build
unzip %{name}-%{version}.zip

%install
mkdir -p %{buildroot}/usr/bin
mv terraform %{buildroot}/usr/bin/terraform

%files
%defattr(755, root, root)
/usr/bin/terraform
