#!/bin/bash

while getopts i: OPT; do
  case "$OPT" in
    i)
      interface="$OPTARG";;
    [?])
      echo "Неизвестный параметр"
      exit 1;;
  esac
done


IFCFG="/etc/sysconfig/network-scripts/ifcfg-$interface"
grep -q "DEFROUTE=no" "$IFCFG" || (
  echo "DEFROUTE=no" >> "$IFCFG"
)
