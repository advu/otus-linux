#!/bin/bash

while getopts g:i:n: OPT; do
  case "$OPT" in
    g)
      GATEWAY="$OPTARG";;
    i)
      INTERFACE="$OPTARG";;
    n)
      NETWORK="$OPTARG";;
    [?])
      echo "Неизвестный параметр $OPT"
      exit 1;;
  esac
done

ROUTE_CONFIG=/etc/sysconfig/network-scripts/route-$INTERFACE
grep -q "$NETWORK via $GATEWAY" "$ROUTE_CONFIG" > /dev/null 2>&1 || (
  echo "$NETWORK via $GATEWAY dev $INTERFACE" >> "$ROUTE_CONFIG"
)
