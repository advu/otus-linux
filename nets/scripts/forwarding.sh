#!/bin/bash
	
sed -i '/^net\.ipv4\.ip_forward.*$/d' /etc/sysctl.conf

if [ ! -f /etc/sysctl.d/router.conf ]; then
  echo "net.ipv4.conf.all.forwarding = 1" > /etc/sysctl.d/router.conf
  sysctl --system
fi
