#!/bin/bash

COUNT=10

while getopts h:c: OPT; do
  case "$OPT" in
    h)
      HOSTS="$HOSTS $OPTARG";;
    c)
      COUNT="$OPTARG";;
    [?])
     echo "Неизвестный параметр"
     exit 1;;
  esac
done

for host in $HOSTS; do
  if [[ $host != +([0-9]).+([0-9]).+([0-9]).+([0-9]) ]]; then
    ip=$(getent hosts $host | awk '{ print $1 }')
  else
    ip=""
  fi
  for ((i=1;i<=$COUNT;i++)); do
    ping -c 1 "$host" >/dev/null 2>&1
    if [[ "$?" == "0" ]]; then
      echo "ping $i [  OK  ] $host $ip"
      break
    else
      if [ $i -eq $COUNT ]; then
        echo "ping $i [ FAIL ] $host $ip" > /dev/stderr
      fi
    fi
  done
done
