#!/bin/bash

while getopts h: OPT; do
  case "$OPT" in
    h)
      HOSTS_LIST="$HOSTS_LIST $OPTARG";;
    [?])
      echo "Неизвестный параметр $OPT"
      exit 1;;
  esac
done


for host in $HOSTS_LIST; do
  host_name=$(echo $host | cut -d '=' -f 1)
  host_ip=$(echo $host | cut -d '=' -f 2)
  grep -q "$host_name" /etc/hosts || echo "$host_ip $host_name" >> /etc/hosts
done
