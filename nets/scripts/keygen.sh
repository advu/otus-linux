#!/bin/bash

KEY_DIR=ssh_keys

print_usage() {
  echo
  echo "$0 -h hostname [-d key-dir]"
  echo
}

while getopts h: OPT; do
  case "$OPT" in
    d)
      KEY_DIR="$OPTARG";;
    h)
      HOST="$OPTARG";;
    [?])
      print_usage
      exit 1;;
  esac
done

if [ ! -d "$KEY_DIR" ]; then
  mkdir -p "$KEY_DIR"
fi

if [ -n "$HOST" ]; then
  KEY_PRIVATE="$KEY_DIR/$HOST"
else
  print_usage
  exit 1
fi

if [ ! -f "$KEY_PRIVATE" ]; then
  ssh-keygen -b 2048 -t rsa -N "" -C "vagrant@$HOST" -f "$KEY_PRIVATE"
  echo "Сгенерированы новые ключи."
fi
