#!/bin/bash

print_usage() {
  echo
  echo Usage:
  echo
  echo "    $0 -h host"
  echo
}

while getopts h: OPT; do
  case "$OPT" in
    h)
      HOST="$OPTARG";;
    [?])
      print_usage
      exit 1;;
  esac
done

if [ -z "$HOST" ]; then
  print_usage
  exit 1
fi

HOSTNAME=$(ssh "$HOST" hostname 2>/dev/null)
if [ -n "$HOSTNAME" ]; then
  IP=$(getent hosts "$HOSTNAME" | awk '{ print $1 }')
  echo "ssh [  OK  ] Connected to $HOSTNAME $IP"
else
  echo "ssh [ FAIL ] Connected to $HOST"
fi
