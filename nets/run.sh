#!/bin/bash

FULL_LOG=full.log
TEST_VM=""

print_usage() {
  echo ""
  echo "Usage:"
  echo ""
  echo "    $0 [-l <log_filename>] [-t vlan|bond]"
  echo ""
  echo "Type (key: -t):"
  echo ""
  echo "    vlan - запуск виртуальных машин для проверки VLAN-ов"
  echo "    bond - запуск вирутальных машин для првоерки bond-а"
  echo ""
  exit
}

while getopts hl:t: OPT; do
  case "$OPT" in
    h) print_usage;;
    l) FULL_LOG="$OPTARG";;
    t)
      case "$OPTARG" in
        vlan) TEST_VM="testServer1 testClient1 testServer2 testClient2";;
        bond) TEST_VM="inetRouter centralRouter";;
        *) print_usage;;
      esac;;
    *) print_usage;;
  esac
done

echo ""
echo "С помощью Vagrant-а запускаются виртуальные машины."
echo ""

vagrant status $TEST_VM

echo ""
echo "Это займёт какое-то время."
echo "На экране будут отображаться только результаты пингов других виртуальных машин."
echo "Для просмотра полного лога Vagrant-а смотрите файл $FULL_LOG"
echo ""

vagrant up --provision $TEST_VM | tee $FULL_LOG | grep ": ping\|: ssh"
