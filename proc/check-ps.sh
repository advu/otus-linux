#!/bin/bash

PS="ps ax"
PS_LOG=ps.log

MYPS=./ps-ax.py
MYPS_LOG=myps.log

$PS > "$PS_LOG"
$MYPS > "$MYPS_LOG"

vimdiff "$PS_LOG" "$MYPS_LOG"

[ -f "$PS_LOG" ] && rm "$PS_LOG"
[ -f "$MYPS_LOG" ] && rm "$MYPS_LOG"
