#!/usr/bin/env python3

import os
import sys

PROC_DIR = "/proc"
LINE_TMP = "{pid:>5} {tty:8} {stat:5}{time:>6} {command}"


def out(text):
    sys.stdout.write(text + '\n')


def get_cmdline(pid, comm):
    cmdline_filename = os.path.join(PROC_DIR, pid, 'cmdline')
    comm_out = "[{}]".format(comm)
    try:
        with open(cmdline_filename, 'r') as f:
            cmdline = f.read()
    except FileNotFoundError:
        return comm_out
    cmdline = cmdline.encode('utf8').replace(b'\x00', b'\x20').decode('utf8')
    cmdline = cmdline.strip()
    return cmdline if cmdline else comm_out


def parse_tty(stat_list):
    tty_nr = int(stat_list[6])
    if tty_nr == 0:
        return '?'
    if tty_nr > 33792:
        return "pts/" + str(tty_nr - 34816)
    return "tty" + str(tty_nr - 1024)


def parse_stat(stat_list, status):
    stat = stat_list[2]
    if int(stat_list[18]) < 0:
        stat += "<"
    if int(stat_list[18]) > 0:
        stat += "N"
    for line in status.split('\n'):
        if "VmLck:" in line:
            if line.split()[1] != '0':
                stat += "L"
            break
    if stat_list[5] == stat_list[0]:
        stat += "s"
    if stat_list[19] != '1':
        stat += "l"
    if stat_list[4] == stat_list[7]:
        stat += "+"
    return stat


def parse_comm(stat_list):
    return stat_list[1].strip("()")


def parse_time(stat_list):
    sec = (int(stat_list[13]) + int(stat_list[14])) // 100
    m, s = divmod(sec, 60)
    return "{0}:{1:0>2}".format(m, s)


def get_stat(pid):
    stat_filename = os.path.join(PROC_DIR, pid, 'stat')
    status_filename = os.path.join(PROC_DIR, pid, 'status')
    try:
        with open(stat_filename, 'r') as f:
            stat = f.read()
        with open(status_filename, 'r') as f:
            status = f.read()
    except FileNotFoundError:
        return ""

    stat_list = stat.split(' ')
    while not stat_list[3].isdigit():
        stat_list[1] += " " + stat_list.pop(2)

    tty = parse_tty(stat_list)
    stat = parse_stat(stat_list, status)
    time = parse_time(stat_list)
    comm = parse_comm(stat_list)

    return tty, stat, time, get_cmdline(pid, comm)


def output_head():
    out(LINE_TMP.format(pid='PID', tty='TTY', stat='STAT', time='TIME',
                        command='COMMAND'))


def output_line(proc):
    out(LINE_TMP.format(**proc))


output_head()
proc_dir_files = os.listdir(PROC_DIR)

for pid in proc_dir_files:
    if pid.isdigit():
        proc = {}
        proc["pid"] = pid
        proc["tty"], proc["stat"], proc["time"], proc["command"] = get_stat(pid)
        output_line(proc)
