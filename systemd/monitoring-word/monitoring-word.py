#!/usr/bin/env python
# -*- coding: utf8 -*-

import yaml
import sys
import os

CONFIG_FILENAME = "/etc/sysconfig/monitoring-word"
REQUIRED_PARAMS = {
    'log_filename': 'Файл для анализа',
    'word': 'Слово для поиска в файле'
}

if not os.path.isfile(CONFIG_FILENAME):
    print("Не найден файл конфигурации {}".format(CONFIG_FILENAME))
    sys.exit(1)

with open(CONFIG_FILENAME, 'r') as config_file:
    try:
        settings = yaml.safe_load(config_file)
    except yaml.YAMLError:
        print("Ошибка загрузки файла конфигурации {}".forman(CONFIG_FILENAME))

if not settings:
    print("Параметры не обнаружены")
    sys.exit(1)

for param_name, description in REQUIRED_PARAMS.items():
    if param_name not in settings or not settings[param_name]:
        print("Не задан параметр {} - {}".format(param_name, description))
        sys.exit(1)

if not os.path.isfile(settings['log_filename']):
    print("Не найден лог файл {}".format(settings['log_filename']))
    sys.exit(1)

with open(settings['log_filename'], 'r') as log_file:
    for line in log_file.readlines():
        if settings['word'] in line:
            print('Искомое слово "{}" в файле "{}" найдено!'.format(
                settings['word'],
                settings['log_filename']
            ))
            sys.exit(0)

print('Слово "{}" в файле "{}" не обнаружено.'.format(
    settings['word'],
    settings['log_filename']
))
