Домашнее задание по Systemd
===========================

1. Написать сервис, который будет раз в 30 секунд мониторить лог на предмет наличия ключевого слова. Файл и слово должны задаваться в /etc/sysconfig
2. Из epel установить spawn-fcgi и переписать init-скрипт на unit-файл. Имя сервиса должно так же называться.
3. Дополнить юнит-файл apache httpd возможностью запустить несколько инстансов сервера с разными конфигами
4. (\*) Скачать демо-версию Atlassian Jira и переписать основной скрипт запуска на unit-файл

1 - monitoring-word
-------------------

После запуска команды `vagrant up monitoring-word` будет создана виртуальная машина, в которой:

Скрипт мониторинга наличия заданного слова в файле: [`/usr/bin/monitoring-word.py`](monitoring-word/monitoring-word.py).
Сервис: [`/etc/systemd/system/monitoring-word.service`](monitoring-word/monitoring-word.service).
Запуск каждые 30 секунд: [`/etc/systemd/system/monitoring-word.timer`](monitoring-word/monitoring-word.timer).
Файл с настройками: [`/etc/sysconfig/monitoring-word`](monitoring-word/monitoring-word).

2 - spawn-fcgi
--------------

После запуска команды `vagrant up spawn-fcgi` будет создана виртуальная машина, в которой:

1.  Будет создано приложение **Hello world!** с поддержкой FastCGI.
2.  Будет установлен пакет **spawn-fcgi**. Для него будет создан
    `/etc/systemd/system/spawn-fcgi.service` для запуска в качестве службы.
3.  Будет установлен web сервер **nginx**, который все запросы на адрес
    `http://192.168.50.12/hi/` будет транслировать **spawn-fcgi**.

Для проверки можно выполнить команду: `curl http://192.168.50.12/hi/`.

Файл [spawn-fcgi.service](spawn-fcgi/ansible/spawn-fcgi.service).

3 - Несколько инстансов apache httpd
------------------------------------

Запустите виртуальную машину командой `vagrant up apache`.

В виртуальной машине запустятся две версии Apache httpd с помощью юнит файла
[httpd@.service](apache/ansible/httpd@.service).

Для проверки работоспособности выполните команды:

* `curl http://192.168.50.13:81/`
* `curl http://192.168.50.13:82/`

Первая команда будет выводить приветственное сообщение от первого web сервера
Apache. Вторая - от второго.
