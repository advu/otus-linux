Занятие 16
==========

Домашнее задание
----------------

Настраиваем бэкапы

Настроить стенд Vagrant с двумя виртуальными машинами server и client.

Настроить политику бэкапа директории /etc с клиента:

1. Полный бэкап - раз в день
2. Инкрементальный - каждые 10 минут
3. Дифференциальный - каждые 30 минут

Запустить систему на два часа. Для сдачи ДЗ приложить list jobs, list files jobid=<id>
и сами конфиги bacula-\*

Дополнительное домашнее задание
-------------------------------

Настроить дополнительные опции - сжатие, шифрование, дедупликация.

Решение
-------

Устанавливаем необходимые ansible роли:

`ansible-galaxy install -r requirements.yml`

Команда `vagrant up` запустит и настроит три виртуальные машины с компонентами Bacula:

 - на ВМ **storage** будет развёрнут Bacula Storage для хранения бэкапов;
 - на ВМ **client** будет развёрнут Bacula Client;
 - на ВМ **director** развернётся Bacule Client, Bacula Console, а также Bacula Director
   с PostgreSQL для управления Bacula в целом.

Конфигурация Bacula Director:

 - Хранение бэкапов на виртуальной машине **storage**;
 - Резервирование каталога `/etc` с виртуальных машин **client** и **director**;
 - Расписание бэкапов в соответствии с заданием;

Настройка системы резервного копирования производится с помощью самописных ansible ролей:

 - [bacula_director](https://galaxy.ansible.com/sedovandrew/bacula_director)
 - [bacula_storage](https://galaxy.ansible.com/sedovandrew/bacula_storage)
 - [bacula_client](https://galaxy.ansible.com/sedovandrew/bacula_client)
 - [bacula_console](https://galaxy.ansible.com/sedovandrew/bacula_console)

Конфигурационные файлы:

- client
    - [bacula-fd.conf](configs_and_outs/client/bacula-fd.conf)
- director
    - [bacula-dir.conf](configs_and_outs/director/bacula-dir.conf)
    - [bacula-fd.conf](configs_and_outs/director/bacula-fd.conf)
    - [bconsole.conf](configs_and_outs/director/bconsole.conf)
- storage
    - [bacula-sd.conf](configs_and_outs/storage/bacula-sd.conf)

Вывод команд в bconsole:

- director
    - [list jobs](configs_and_outs/director/list_jobs.txt)
    - [list files jobid=1](configs_and_outs/director/list_files_jobid_01.txt)
    - [list files jobid=2](configs_and_outs/director/list_files_jobid_01.txt)
    - [list files jobid=24](configs_and_outs/director/list_files_jobid_01.txt)
