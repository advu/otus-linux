#!/bin/bash

print_usage() {
  echo
  echo "Usage: $0 -l ubuntu-18.04-bionic-desktop-amd64.iso"
  echo
  echo "    -l - образ LiveCD"
}

while getopts l: OPT; do
  case $OPT in
    l)
      LIVE_CD_ISO=$OPTARG ;;
    [?])
      print_usage
      exit 1 ;;
  esac
done

if [ -z "$LIVE_CD_ISO" ]; then
  echo "Параметр -l является обязательным."
  print_usage
  exit 1
fi

vagrant up
vagrant halt

VM_ID=$(cat .vagrant/machines/otuslinux/virtualbox/id)

VBoxManage storageattach $VM_ID \
  --storagectl SATA \
  --port 0 \
  --device 0 \
  --type dvddrive \
  --medium $LIVE_CD_ISO

VBoxManage startvm $VM_ID

echo
echo 'Виртуальная машина должна была запуститься с LiveCD'
echo
echo 'Для уменьшения размера корневого раздела выполните последовательно'
echo 'следующие команды в запущенной виртуальной машине:'
echo
echo 'sudo mkdir /mnt/hdd'
echo 'sudo mount /dev/sdb /mnt/hdd'
echo 'sudo bash /mnt/hdd/small-root-xfs.sh'
echo
echo 'Как выполнятся скрипты на виртуальной машине, выключите её, затем нажмите'
echo '"Enter" в основной консоли, и ваша виртуальная машина загрузится с основного диска'

read

VBoxManage controlvm $VM_ID poweroff

VBoxManage storageattach $VM_ID \
  --storagectl SATA \
  --port 0 \
  --device 0 \
  --type dvddrive \
  --medium none

vagrant up

echo
echo 'Для проверки работы snapshot-а выполните следующие команды внутри виртуальной машины:'
echo
echo 'cd /'
echo 'chk-snapshot-work.sh'
echo
echo 'Для создания btrfs в каталоге /opt и backup-а каталога /opt/data запустите:'
echo
echo 'sudo mk-btrfs.py'
echo
echo 'Заходим на виртуальную машину'
echo

vagrant ssh

