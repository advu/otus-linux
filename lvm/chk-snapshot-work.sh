#!/bin/bash

# Name of "/home" snapshot
HOME_LV_SNAPSHOT=LogVolHomeSnapshot

# Size of "/home" snapshot 
HOME_SNAPSHOT_SIZE=1G

# Name of "/home" logical volume
HOME_LV=LogVolHome

print_usage() {
  echo
  echo "Usage: $0 -l $HOME_LV -n $HOME_LV_SHAPSHOT -s $HOME_SNAPSHOT_SIZE"
  echo
  echo "    -l - имя логического тома"
  echo "    -n - имя snapshot-а"
  echo "    -s - размер snapshot-а"
}

list_files() {
  ls $BASE_DIR/file_*.txt
  echo
}

while getopts l:n:s: OPT; do
  case $OPT in
    l)
      HOME_LV=$OPTARG ;;
    n)
      HOME_LV_SNAPSHOT=$OPTARG ;;
    s)
      HOME_SNAPSHOT_SIZE=$OPTARG ;;
    [?])
      print_usage
      exit 1 ;;
  esac
done

HOME_DEV=/dev/mapper/VolGroup00-$HOME_LV
HOME_DEV_SNAPSHOT=/dev/mapper/VolGroup00-$HOME_LV_SNAPSHOT

BASE_DIR=/home/vagrant
for i in $(seq 10); do
  echo "Data for file № $i" > $BASE_DIR/file_$i.txt
done

echo "Список файлов после создания:"
list_files

echo "Создание snapshot-а"
sudo lvcreate --snapshot -L $HOME_SNAPSHOT_SIZE --name $HOME_LV_SNAPSHOT $HOME_DEV
echo

for i in $(seq 3 8); do
  rm $BASE_DIR/file_$i.txt
done

echo "Список файлов после удаления:"
list_files

echo "Восстановление из snapshot-а"
sudo umount /home
sudo lvconvert --merge $HOME_DEV_SNAPSHOT
sudo mount /home
echo

echo "Список файлов после восстановления из snapshot-а:"
list_files

