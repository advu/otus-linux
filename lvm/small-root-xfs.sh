#!/bin/bash

VG=VolGroup00

# Size of "/" logical volume
ROOT_SIZE=8G
ROOT_DIR=/mnt/root
ROOT_DEV=/dev/mapper/VolGroup00-LogVol00

# Size of "/var" logical volume
VAR_SIZE=4G
VAR_DIR=/mnt/root/var
# Name of "/var" logical volume
VAR_LV=LogVolVar
VAR_DEV=/dev/mapper/VolGroup00-$VAR_LV
# Mirrors count of "/var" logical volume
VAR_MIRRORS_COUNT=1

# Size of "/home" logical volume
HOME_SIZE=2G
HOME_DIR=/mnt/root/home
# Name of "/home" logical volume
HOME_LV=LogVolHome
HOME_DEV=/dev/mapper/VolGroup00-$HOME_LV
# Name of "/home" snapshot
HOME_LV_SNAPSHOT=LogVolHomeSnapshot
# Size of "/home" snapshot 
HOME_SNAPSHOT_SIZE=1G

TMP_DIR=/mnt/hdd/root

# Clean disk for temp data
SECOND_DISK=/dev/sdc
SECOND_PARTITION=${SECOND_DISK}1

LOG_FILE=${0%.*}.log

chkstatus () {
if [ $1 -eq 0 ]; then
  echo -e ' \033[1;32m[ OK ]\033[0m'
else
  echo -e ' \033[1;31m[Fail]\033[0m'
fi
}

if [ ! -d $ROOT_DIR ]; then
  echo -n "Create dir $ROOT_DIR"
  mkdir -p $ROOT_DIR
  chkstatus $?
fi

if [ ! -d $TMP_DIR ]; then
  echo -n "Create temp dir $TMP_DIR"
  mkdir -p $TMP_DIR
  chkstatus $?
fi

echo -n 'Mount root partition'
mount $ROOT_DEV $ROOT_DIR > $LOG_FILE 2>&1
chkstatus $?

echo -n 'Copy data from root to temp dir'
rsync -aAX ${ROOT_DIR}/* $TMP_DIR >> $LOG_FILE 2>&1
chkstatus $?

echo -n 'Unmount root partition'
umount $ROOT_DIR >> $LOG_FILE 2>&1
chkstatus $?

echo -n 'Resize root partition'
lvresize -L $ROOT_SIZE -f $ROOT_DEV >> $LOG_FILE 2>&1
chkstatus $?
echo -n 'Make FS on root partition'
mkfs.xfs -f $ROOT_DEV >> $LOG_FILE 2>&1
chkstatus $?

echo -n 'Create partition on second drive'
echo -e "n\n\n\n\n\nw\nY\n" | gdisk $SECOND_DISK >> $LOG_FILE 2>&1
chkstatus $?
echo -n 'Add partition on second drive to volume group'
vgextend $VG $SECOND_PARTITION >> $LOG_FILE 2>&1
chkstatus $?
echo -n 'Create logical volume with mirror for /var'
lvcreate --mirrors $VAR_MIRRORS_COUNT -L $VAR_SIZE --name $VAR_LV $VG >> $LOG_FILE 2>&1
chkstatus $?
echo -n 'Create file system for /var'
mkfs.ext4 $VAR_DEV >> $LOG_FILE 2>&1
chkstatus $?

echo -n 'Create home partition'
lvcreate -L $HOME_SIZE --name $HOME_LV $VG >> $LOG_FILE 2>&1
chkstatus $?
echo -n 'Create file system for /home'
mkfs.ext4 $HOME_DEV >> $LOG_FILE 2>&1
chkstatus $?

echo -n 'Mount root partition'
mount $ROOT_DEV $ROOT_DIR >> $LOG_FILE 2>&1
chkstatus $?

if [ ! -d $VAR_DIR ]; then
  echo -n "Create dir $VAR_DIR for var"
  mkdir $VAR_DIR
  chkstatus $?
fi
echo -n 'Mount var partition'
mount $VAR_DEV $VAR_DIR >> $LOG_FILE 2>&1
chkstatus $?

if [ ! -d $HOME_DIR ]; then
  echo -n "Create dir $HOME_DIR for home"
  mkdir $HOME_DIR
  chkstatus $?
fi
echo -n 'Mount home partition'
mount $HOME_DEV $HOME_DIR >> $LOG_FILE 2>&1
chkstatus $?

echo -n 'Copy data from temp dir to root'
rsync -aAX ${TMP_DIR}/* $ROOT_DIR >> $LOG_FILE 2>&1
chkstatus $?

echo -n 'Add /var to /etc/fstab'
echo "$VAR_DEV /var ext4 defaults 0 0" >> $ROOT_DIR/etc/fstab
chkstatus $?
echo -n 'Add /home to /etc/fstab'
echo "$HOME_DEV /home ext4 defaults 0 0" >> $ROOT_DIR/etc/fstab
chkstatus $?

echo -n "Unmount $VAR_DIR"
umount $VAR_DIR >> $LOG_FILE 2>&1
chkstatus $?
echo -n "Unmount $HOME_DIR"
umount $HOME_DIR >> $LOG_FILE 2>&1
chkstatus $?
echo -n "Unmount $ROOT_DIR"
umount $ROOT_DIR >> $LOG_FILE 2>&1
chkstatus $?

echo "Log - $LOG_FILE"
echo 'Виртуальную машину следует выключить, после чего нажать "Enter" в основной консоли'
