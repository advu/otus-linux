#!/usr/bin/env python
# -*- coding: utf8 -*-

import subprocess as sp
import os

btrfs_device = '/dev/sdd'
btrfs_root_folder = '/opt'
btrfs_subvolume_data = os.path.join(btrfs_root_folder, 'data')
btrfs_snapshot_data = os.path.join(btrfs_root_folder, 'data-snapshot')

tmp_filename = 'data-for-test'

btrfs_device_backup = '/dev/sde'
btrfs_folder_backup = '/mnt/backup'

sp.check_call(['mkfs.btrfs', btrfs_device])
sp.check_call(['mount', btrfs_device, btrfs_root_folder])
sp.check_call(['btrfs', 'subvolume', 'create', btrfs_subvolume_data])

with open(os.path.join(btrfs_subvolume_data, tmp_filename), 'w') as f:
    f.write('Test text data in file {}\n'.format(tmp_filename))

sp.check_call(['btrfs', 'subvolume', 'snapshot', '-r', btrfs_subvolume_data,
               btrfs_snapshot_data])
sp.check_call(['sync'])

sp.check_call(['mkfs.btrfs', btrfs_device_backup])

os.makedirs(btrfs_folder_backup)

sp.check_call(['mount', btrfs_device_backup, btrfs_folder_backup])

send = sp.Popen(['btrfs', 'send', btrfs_snapshot_data], stdout=sp.PIPE)
sp.check_call(['btrfs', 'receive', btrfs_folder_backup], stdin=send.stdout)

with open('/etc/fstab', 'r+') as f:
    fstab = f.read()
    for path, dev in ((btrfs_root_folder, btrfs_device),
                      (btrfs_folder_backup, btrfs_device_backup)):
        line_tpl = '{dev} {path} btrfs defaults 0 0'
        line = line_tpl.format(dev=dev, path=path)
        if line not in fstab:
            f.write(line + '\n')

output_tpl = """
После работы скрипта вы должны были получить:

- файловую систему btrfs на устройстве {btrfs_device} смонтированную в папку
  {btrfs_root_folder}
- subvolume в папке {btrfs_subvolume_data}
- snapshot в папке {btrfs_snapshot_data}
- файловую систему btrfs на устройстве {btrfs_device_backup} смонтированную
  в папку {btrfs_folder_backup}
- backup snapshot-а {btrfs_snapshot_data} в папке {btrfs_folder_backup}
"""

print(output_tpl.format(btrfs_device=btrfs_device,
                        btrfs_root_folder=btrfs_root_folder,
                        btrfs_subvolume_data=btrfs_subvolume_data,
                        btrfs_snapshot_data=btrfs_snapshot_data,
                        btrfs_device_backup=btrfs_device_backup,
                        btrfs_folder_backup=btrfs_folder_backup))
