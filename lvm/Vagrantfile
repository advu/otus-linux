# -*- mode: ruby -*-
# vim: set ft=ruby :

MACHINES = {
    :otuslinux => {
        :box_name => "centos/7",
        :ip_addr => '192.168.11.101',
        :disks => {
            :sata1 => {
                :dfile => './sata1.vdi',
                :size => 20000,
                :port => 1
            },
            :sata2 => {
                :dfile => './sata2.vdi',
                :size => 20000, # Megabytes
                :port => 2
            },
            :sata3 => {
                :dfile => './sata3.vdi',
                :size => 500,
                :port => 3
            },
            :sata4 => {
                :dfile => './sata4.vdi',
                :size => 500, # Megabytes
                :port => 4
            }
        }
    }
}

Vagrant.configure("2") do |config|

    MACHINES.each do |boxname, boxconfig|

        config.vm.define boxname do |box|
            box.vm.box = boxconfig[:box_name]
            box.vm.box_check_update = false
            box.vm.host_name = boxname.to_s
            box.vm.synced_folder ".", "/vagrant", disabled: true
            box.vm.network "private_network", ip: boxconfig[:ip_addr]

            box.vm.provider :virtualbox do |vb|
                vb.customize ["modifyvm", :id, "--memory", "2048"]

                # TODO: Не знаком с Ruby, поэтому пока так.
                unless File.exist?(boxconfig[:disks][:sata1][:dfile])
                    vb.customize ["storagectl", :id, "--name", "SATA", "--add",
                                  "sata" ]
                end

                boxconfig[:disks].each do |dname, dconf|
                    unless File.exist?(dconf[:dfile])
                        vb.customize ['createhd', '--filename', dconf[:dfile],
                                      '--variant', 'Standard', '--size', dconf[:size]]
                        vb.customize ['storageattach', :id,  '--storagectl', 'SATA',
                                      '--port', dconf[:port], '--device', 0, '--type',
                                      'hdd', '--medium', dconf[:dfile]]
                    end
                end
            end

            box.vm.provision "shell", inline: <<-SHELL
                mkdir -p ~root/.ssh
                cp ~vagrant/.ssh/auth* ~root/.ssh
            SHELL

            box.vm.provision "file" do |file|
                file.source = "small-root-xfs.sh"
                file.destination = "/tmp/small-root-xfs.sh"
            end

            box.vm.provision "file" do |snapshot|
                snapshot.source = "chk-snapshot-work.sh"
                snapshot.destination = "/tmp/chk-snapshot-work.sh"
            end

            box.vm.provision "file" do |btrfs|
                btrfs.source = "mk-btrfs.py"
                btrfs.destination = "/tmp/mk-btrfs.py"
            end

            box.vm.provision "shell", inline: <<-SHELL
                mkfs.xfs /dev/sdb
                mkdir /mnt/hdd
                mount /dev/sdb /mnt/hdd
                mv /tmp/small-root-xfs.sh /mnt/hdd/
                mv /tmp/chk-snapshot-work.sh /usr/local/bin/
                chmod +x /usr/local/bin/chk-snapshot-work.sh
                mv /tmp/mk-btrfs.py /usr/bin/mk-btrfs.py
                chmod +x /usr/bin/mk-btrfs.py
            SHELL
        end
    end
end

