#!/bin/bash

while getopts v: OPT; do
  case "$OPT" in
    v)
      if [[ $OPTARG != +([0-9]).+([0-9]).+([0-9]) ]]; then
        echo Bad version
        exit 1
      fi
      KERNEL_VERSION=$OPTARG ;;
    [?])
      echo Usage: $0 -v 3.16.56
      exit 1 ;;
  esac
done

KERNEL_FILE=linux-${KERNEL_VERSION}.tar.xz

yum update -y
yum groupinstall -y "Development Tools"
yum install -y bc

cd /usr/src
if [ ! -f ${KERNEL_FILE} ]; then
  if [ -f /vagrant/${KERNEL_FILE} ]; then
    echo "Copy linux kernel from /vagrant"
    cp /vagrant/${KERNEL_FILE} .
  else
    echo "Download linux kernel"
    curl https://cdn.kernel.org/pub/linux/kernel/v${KERNEL_VERSION%%.*}.x/${KERNEL_FILE} -O ${KERNEL_FILE}
  fi
fi

if [ ! -d linux-${KERNEL_VERSION} ]; then
  tar -xf ${KERNEL_FILE}
fi

if [ ! -L linux ]; then
  ln -s linux-${KERNEL_VERSION} linux
fi

cd linux

make clean && make mrproper

cp /boot/config-$(uname -r) .config
yes "" | make oldconfig

make rpm

# Install new kernel
rpm -ivh /root/rpmbuild/RPMS/$(uname -i)/kernel-${KERNEL_VERSION}-1.$(uname -i).rpm

# Load new kernel
sed -i 's/GRUB_DEFAULT=.*/GRUB_DEFAULT=0/' /etc/default/grub
grub2-mkconfig -o /boot/grub2/grub.cfg
