#!/bin/bash

while getopts f:s:h:p: OPT; do
  case "$OPT" in
    f)
      EMAIL=$OPTARG;;
    s)
      PASSWORD=$OPTARG;;
    h)
      SMTP_HOST=$OPTARG;;
    p)
      SMTP_PORT=$OPTARG;;
    [?])
      echo "Неизвестный параметр $OPT"
      exit 1;;
  esac
done

if [[ -z "$EMAIL" ]]; then
  echo "Не задан почтовый адрес отправителя"
  exit 1
fi

if [[ -z "$PASSWORD" ]]; then
  echo "Не задан пароль для почтового адреса отправителя"
  exit 1
fi

if [[ -z "$SMTP_HOST" ]]; then
  echo "Не задан хост smtp сервера"
  exit 1
fi

if [[ -z "$SMTP_PORT" ]]; then
  echo "Не задан порт smtp сервера"
  exit 1
fi

yum install -y mailx cyrus-sasl cyrus-sasl-lib cyrus-sasl-plain

BEGIN_LINE="# BEGIN MY POSTFIX CONFIG"
if ! [ $(grep "^$BEGIN_LINE$" /etc/postfix/main.cf > /dev/null 2>&1) ]; then
  cat >> /etc/postfix/main.cf <<EOF
$BEGIN_LINE 
relayhost = $SMTP_HOST:$SMTP_PORT
smtp_use_tls = yes
smtp_sasl_auth_enable = yes
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_sasl_security_options = noanonymous
smtp_tls_security_level = may
EOF
  RESTART_POSTFIX="yes"
fi

if ! [ $(grep "^$SMTP_HOST:$SMTP_PORT" /etc/postfix/sasl_passwd > /dev/null 2>&1) ]; then
  echo "$SMTP_HOST:$SMTP_PORT $EMAIL:$PASSWORD" >> /etc/postfix/sasl_passwd
  postmap /etc/postfix/sasl_passwd
  RESTART_POSTFIX="yes"
fi

if [[ "$RESTART_POSTFIX" == "yes" ]]; then
  systemctl restart postfix.service
fi
