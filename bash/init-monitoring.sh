#!/bin/bash

SYSTEMD_SERVICE_FILE=http-monitoring.service

while getopts f:t: OPT; do
  case "$OPT" in
    f)
      EMAIL_FROM="$OPTARG";;
    t)
      EMAIL_TO="$OPTARG";;
    [?])
      echo "Неизвестный параметр: -$OPT $OPTARG"
      exit 1;;
  esac
done

if [[ -z "$EMAIL_FROM" ]]; then
  echo "Не задан почтовый адрес отправителя"
  exit 1
fi

if [[ -z "$EMAIL_TO" ]]; then
  echo "Не задан почтовый адрес получателя"
  exit 1
fi

chmod +x /home/vagrant/monitoring.sh

cat > "/etc/systemd/system/$SYSTEMD_SERVICE_FILE" <<EOF_SYSTEMD_SERVICE
[Unit]
Description=Monitoring python HTTP file server
After=http-files.service

[Service]
Type=simple
ExecStart=/home/vagrant/monitoring.sh -t "$EMAIL_TO" -f "$EMAIL_FROM"
WorkingDirectory=/home/vagrant
Restart=always

[Install]
WantedBy=multi-user.target
EOF_SYSTEMD_SERVICE

systemctl daemon-reload
systemctl enable ${SYSTEMD_SERVICE_FILE}
systemctl start ${SYSTEMD_SERVICE_FILE}
