#!/bin/bash

USER=httpfiles
ROOT_DIR=/opt/http-files
SYSTEMD_SERVICE_FILE=http-files.service
PORT=8000

while getopts u:d:p: OPT; do
  case "$OPT" in
    u)
      USER=$OPTARG;;
    d)
      ROOT_DIR=$OPTARG;;
    p)
      PORT=$OPTARG;;
    [?])
      echo "Неизвестный параметр $OPT"
      exit 1;;
  esac
done

if ! [ $(grep "^$USER:" /etc/passwd > /dev/null 2>&1) ]; then
  useradd --no-create-home --user-group --shell /sbin/nologin --system $USER
fi

if ! [ -f "$ROOT_DIR" ]; then
  mkdir -p "$ROOT_DIR"
fi
chown $USER:$USER "$ROOT_DIR"

cat > /etc/systemd/system/$SYSTEMD_SERVICE_FILE <<EOF_SYSTEMD_SERVICE
[Unit]
Description=Python HTTP file server

[Service]
Type=simple
ExecStart=/usr/bin/python -m SimpleHTTPServer $PORT
WorkingDirectory=$ROOT_DIR
User=$USER
Restart=always

[Install]
WantedBy=multi-user.target
EOF_SYSTEMD_SERVICE

systemctl daemon-reload
systemctl enable $SYSTEMD_SERVICE_FILE
systemctl start $SYSTEMD_SERVICE_FILE
