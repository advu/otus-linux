#!/bin/bash

UNIT=http-files.service
IP_DB_FILE=ip.db
IP_DB=ip
IP_DB_IP_NAME=ip
IP_DB_IP_COUNT=count

while getopts d:f:t:u: OPT; do
  case $OPT in
    d)
      IP_DB_FILE="$OPTARG";;
    f)
      EMAIL_FROM="$OPTARG";;
    t)
      EMAIL_TO="$OPTARG";;
    u)
      UNIT="$OPTARG";;
    [?])
      echo "Неизвестный параметр: -$OPT $OPTARG"
      exit 1;;
  esac
done

if [[ -z "$EMAIL_FROM" ]]; then
  echo "Не задан почтовый адрес отправителя"
  exit 1
fi

if [[ -z "$EMAIL_TO" ]]; then
  echo "Не задан почтовый адрес получателя"
  exit 1
fi

email() {
  echo "Request from new ip address: $*" | mail -s "http server" -r $EMAIL_FROM $EMAIL_TO
}

increase_ip_count() {
  ip_count=$(sqlite3 ip.db "SELECT $IP_DB_IP_COUNT FROM $IP_DB WHERE $IP_DB_IP_NAME == \"$1\";")
  if [[ -n "$ip_count" ]]; then
    ip_count_increased=$[ ip_count + 1 ]
    sqlite3 "$IP_DB_FILE" "UPDATE $IP_DB
                           SET $IP_DB_IP_COUNT = $ip_count_increased
                           WHERE $IP_DB_IP_NAME == \"$1\";"
    return $ip_count_increased
  else
    sqlite3 "$IP_DB_FILE" "INSERT INTO $IP_DB VALUES(\"$1\", 1);"
    return 1
  fi
}

echo "Starting script"

echo "Checking database"
if ! [ -f "$IP_DB_FILE" ]; then
  echo "Creating database"
  sqlite3 "$IP_DB_FILE" "CREATE TABLE $IP_DB($IP_DB_IP_NAME text primary key,
                                             $IP_DB_IP_COUNT integer);"
fi

echo "Running monitoring"
journalctl --lines 0 --follow _SYSTEMD_UNIT=${UNIT} | while read line
do
  echo $line
  ip=$(echo $line | awk '/[012]?[0-9]?[0-9]\.[012]?[0-9]?[0-9]\.[012]?[0-9]?[0-9]\.[012]?[0-9]?[0-9]/ { print $6 }')
  if [[ -n "$ip" ]]; then
    increase_ip_count "$ip"
    if [[ "$?" == "1" ]]; then
      email "$ip"
    fi
  fi
done
