#!/bin/bash

DISKS_COUNT=2
RAID_DEVICE=/dev/md0
RAID_LEVEL=1
while getopts c:d:l: OPT; do
  case "$OPT" in
    c)
      DISKS_COUNT=$OPTARG ;;
    d)
      RAID_DEVICE=$OPTARG ;;
    l)
      RAID_LEVEL=$OPTARG ;;
    [?])
      echo Usage: $0 -l 5 -d /dev/md0
      exit 1 ;;
  esac
done

if [ "$DISKS_COUNT" -lt "2" ]; then
  echo Too few drives are specified
  exit 1
fi
if [ "$RAID_LEVEL" -eq "5" ]; then
  if [ "$DISKS_COUNT" -lt "3" ]; then
    echo Too few drives are specified for raid level 5
    exit 1
  fi
fi
if [ "$RAID_LEVEL" -eq "6" ]; then
  if [ "$DISKS_COUNT" -lt "4" ]; then
    echo Too few drives are specified for raid level 6
    exit 1
  fi
fi

declare -A RAID_LEVELS=( [0]=1 [1]=1 [4]=1 [5]=1 [6]=1 [10]=1 )
if [ ! ${RAID_LEVELS[$RAID_LEVEL]} ]; then
  echo Bad raid level
  exit 1
fi

if [ -b $RAID_DEVICE ]; then
  echo Device $RAID_DEVICE already exist
  exit 0
fi

DISKS_LETTERS_ALL=( b c d e f g h i j k l m n o p q r s t u v w x y z)
DISKS_LETTERS=${DISKS_LETTERS_ALL[@]::$DISKS_COUNT}
DEVICES=""
DELIMETER_DEVICE=""

for i in ${DISKS_LETTERS[@]}; do
  device=/dev/sd${i}

  DEVICES+=${DELIMETER_DEVICE}${device}1
  DELIMETER_DEVICE=" "

  echo -e 'n\n1\n\n\nfd00\nw\nY\n' | gdisk ${device}
  #mkfs.ext3 ${device}1
done

yes "y" | mdadm --create ${RAID_DEVICE} --level=${RAID_LEVEL} \
                --raid-devices=${DISKS_COUNT} ${DEVICES}

