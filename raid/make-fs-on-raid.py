#!/usr/bin/env python3

import argparse
import os
import subprocess


def get_device_part(device, num):
    return device + 'p' + str(num)


def check_fs_util(fs):
    paths = os.getenv('PATH')
    for path in paths.split(':'):
        util = os.path.join(path, "mkfs." + fs)
        if os.path.exists(util):
            return True
    return False


parser = argparse.ArgumentParser()
parser.add_argument('--raid-device', '-d')
parser.add_argument('--folder', '-f', action='append', nargs=3,
                    metavar=('path', 'size', 'fs'))
args = parser.parse_args()


mk_part_str = ''
for num, (path, size, fs) in enumerate(args.folder, 1):
    if not os.path.exists(get_device_part(args.raid_device, num)):
        mk_part_tpl = 'n\n{np}\n\n+{size}\n8300\n'
        mk_part_str += mk_part_tpl.format(np=num, size=size)

if mk_part_str:
    mk_part_str += 'w\nY\n'
    o = subprocess.check_output(["gdisk", args.raid_device],
                                input=mk_part_str.encode('utf8'))
    print(o.decode('utf8'))

for num, (path, size, fs) in enumerate(args.folder, 1):
    if not check_fs_util(fs):
        continue

    if not os.path.ismount(path):
        device = get_device_part(args.raid_device, num)
        subprocess.check_call(["mkfs." + fs, device])

    if not os.path.exists(path):
        os.mkdir(path)

    with open('/etc/fstab', 'r+') as f:
        if path not in f.read():
            mount_tpl = "\n{} {} {} defaults 0 0\n"
            mount_str = mount_tpl.format(device, path, fs)
            f.write(mount_str)

    if not os.path.ismount(path):
        subprocess.check_call(["mount", path])
