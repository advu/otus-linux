Домашнее задание
----------------

VPN

1.  Между двумя виртуалками поднять vpn в режимах
        - tun
        - tap
    Прочуствовать разницу.

2.  Поднять RAS на базе OpenVPN с клиентскими сертификатами, подключиться с локальной машины на виртуалку

3.  \* Самостоятельно изучить, поднять ocserv и подключиться с хоста к виртуалке

Реализация
----------

В рамках задания была создана [ansible роль для настройки openvpn](https://galaxy.ansible.com/sedovandrew/openvpn).

По умолчанию роль использует параметр `openvpn_dev` в значении `tun`. В файле [vpn/group_vars/all.yml](vpn/group_vars/all.yml)
можно переопределить данное поведение, раскоментировав указанный параметр.

Необходимые ключи будут созданы в директории `vpn/keys`.

Установка зависимостей:

```bash
ansible-galaxy install -r requirements.yml
```

Запуск:

```bash
vagrant up
```

Проверить работоспособность можно командой:

```bash
vagrant ssh -c 'ping 10.8.0.1' client
```

Проверить интерфейс на клиенте:

```bash
vagrant ssh -c 'ip a show dev tun0' client
```

или сервере:

```bash
vagrant ssh -c 'ip a show dev tun0' server
```
